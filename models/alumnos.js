import conexion from "../models/conexion.js";

var alumnosDb = {}

alumnosDb.insertar = function insertar(alumno){
    return new Promise((resolve, reject)=>{
        // consulta
        let sqlConsulta = "Insert into alumnos set ?";
        conexion.query(sqlConsulta, alumno, function(err, res){
            if (err) {
                console.log("Surgio un error ",err.message);
                reject(err);
            } else {
                const alumnoInsert = {
                    id:res.insertId,
                };
                resolve(alumnoInsert);
            }
        });
    });
}

alumnosDb.mostrarTodos = function mostrarTodos() {
    return new Promise((resolve, reject)=> {
        var sqlConsulta = "select * from alumnos";
        conexion.query(sqlConsulta, null, function(err, res) {
            if (err) {
                console.log("Surgio un error");
                reject(err);
            } else {
                resolve(res);
            }
        });
    });
}

alumnosDb.consultarMatricula = function consultarMatricula(matricula){
    return new Promise((resolve, reject)=>{
        var sqlConsulta = "SELECT * FROM alumnos WHERE matricula = ?";
        conexion.query(sqlConsulta, [matricula], function(err, res){
            if (err){
                console.log("Surgio un error", err.message);
                reject(err);
            } else {
                resolve(res);
            }
        })
    })
}

alumnosDb.borrrarMatricula = function borrarMAtricula(matricula){
    return new Promise((resolve, reject)=>{
        var sqlConsulta = "DELETE * FROM alumnos WHERE matricula = ?";
        conexion.query(sqlConsulta, [matricula], function(err, res){
            if (err){
                console.log("Surgio un error", err.message);
                reject(err);
            } else {
                resolve(res);
            }
        })
    })
}

alumnosDb.actualizarMatricula = function actualizarMatricula(matricula){
    return new Promise((resolve, reject)=>{
        var sqlConsulta = "UPDATE alumnos SET matricula = ?, nombre = ?, domicilio = ?, especialidad = ?, sexo = ? WHERE matricula = ?";
        conexion.query(sqlConsulta, [nombre, domicilio, especialidad, sexo, matricula], function(err, res){
            if (err){
                console.log("Surgio un error", err.message);
                reject(err);
            } else {
                resolve(res);
            }
        })
    })
}
    
export default alumnosDb;